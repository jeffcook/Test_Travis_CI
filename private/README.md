# Private Directory

Any files in this directory structure should not be committed to git.
Files in the /private/ directory will be committed, but anything in a sub-folder will not.
This will allow users to build their own tests and patterns without having to commit them to the repository for the tool.

## How to use

You can create a ./private/tests and a ./private/patterns folder.
These folders will get loaded and run durning testing.
It is your responsibility to make sure nothing in these folders are ever committed tot he git repository.
