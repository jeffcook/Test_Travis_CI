#!/usr/bin/env ruby
## Code example from
## https://fabianlee.org/2016/05/25/logstash-testing-logstash-grok-patterns-locally-on-linux/
require "rubygems"
require "grok-pure" # for ruby 1.9
require "pp"


## script inputs

# folders to test
folders = []

# add folders from arguments if any
ARGV.each do|a|
  if File.directory? a
    folders << a
  else 
    puts "ERROR: #{a} is not a directory."
  end
end
patternfolders = []
testfolders = []
folders.each do|a|
  #puts "folder: #{a}"
  patternfolders.push( "#{a}/patterns/*")
  testfolders.push( "#{a}/tests/**/*")
end

grok = Grok.new
# more patterns available:
# https://github.com/logstash-plugins/logstash-patterns-core/tree/master/patterns
# Load all files in the ./patterns folder
grokexit = 1

puts "Loading grok pattern files..."
patternfolders.each do |folder_name|
  # puts "     #{folder_name}"
  patternfiles = Dir[folder_name]
  # puts patternfiles
  patternfiles.each do |file_name|
    if !File.directory? file_name
      puts "     #{file_name}"
      grok.add_patterns_from_file(file_name)
    end
  end  
end

puts "Loading test files..."
testfolders.each do |folder_name|
  testfiles = Dir[folder_name]
  testfiles.each do |dir_name|
    if File.directory? dir_name
      puts "     #{dir_name}"
      # puts "          file_filter: #{dir_name}/filter"
      filtermatch = File.read("#{dir_name}/filter")
      # puts "          FILTER: #{filtermatch}"

      # puts "          file_log: #{dir_name}/log"
      log = File.read("#{dir_name}/log")
      # puts "          LOG: #{log}"

      expectfile = "#{dir_name}/expect"
      if  File.exists?(expectfile)
        expectedresult = File.read(expectfile)
        # puts "          EXPECTED RESULT: \n#{expectedresult}"
      else
        expectedresult = nil
      end

      grok.compile(filtermatch)
      matched = grok.match(log)

      if matched
        result = matched.captures
        if "#{result}" == "#{expectedresult}"
          puts "          Matched expected result.\n\n"
        else
          puts "          FILTER: #{filtermatch}"
          puts "          MATCH:\n\n#{result}\n\n"
        end
      else
        puts "          SORRY, no match!"
        grokexit = 0
      end
    end
  end
end
puts

grokexit

