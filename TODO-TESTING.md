# TODO - Testing

* Read/process all log files in a log folder.
* Verify parsed output matches expected output.
* Add Logstash config file.
* Load Logstash to review the config file.
* Use Logstash to parse the patterns and run the filters.
